# Player Order

| #   | Name       | Food group      | Fruits | Root vegetables | Score |
| --- | ---------- | --------------- | ------ | --------------- | ----- |
| 1   | Sonja      | Grains          | 0      | 1               | 17    |
| 2   | Jbt        | Fruits          | 1      | 0               | 15    |
| 3   | Sateviss   | Grains          | 0      | 0               | 9     |
| 4   | Coinkydink | Fruits          | 0      | 0               | 21    |
| 5   | Haunty     | Root vegetables | 0      | 0               | 0     |
| 6   | Ned        | Fruits          | 0      | 0               | 4     |

# Current Ruleset

## Immutable Rules

**101.** All players must always abide by all the rules then in effect, in the form in which they are then in effect. The rules in the Initial Set are in effect whenever a game begins. The Initial Set consists of Rules 101-116 (immutable) and 201-213 (mutable).

**102.** Initially rules in the 100's are immutable and rules in the 200's are mutable. Rules subsequently enacted or transmuted (that is, changed from immutable to mutable or _vice versa_) may be immutable or mutable regardless of their numbers, and rules in the Initial Set may be transmuted regardless of their numbers.

**103.** A rule-change is any of the following: (1) the enactment, repeal, or amendment of a mutable rule; (2) the enactment, repeal, or amendment of an amendment of a mutable rule; or (3) the transmutation of an immutable rule into a mutable rule or _vice versa_.

(Note: This definition implies that, at least initially, all new rules are mutable; immutable rules, as long as they are immutable, may not be amended or repealed; mutable rules, as long as they are mutable, may be amended or repealed; any rule of any status may be transmuted; no rule is absolutely immune to change.)

**104.** All rule-changes proposed in the proper way shall be voted on. They will be adopted if and only if they receive the required number of votes.

**105.** Every player is an eligible voter. Every eligible voter must participate in every vote on rule-changes.

**106.** All proposed rule-changes shall be written down before they are voted on. If they are adopted, they shall guide play in the form in which they were voted on.

**107.** No rule-change may take effect earlier than the moment of the completion of the vote that adopted it, even if its wording explicitly states otherwise. No rule-change may have retroactive application.

**108.** Each proposed rule-change shall be given a number for reference. The numbers shall begin with 301, and each rule-change proposed in the proper way shall receive the next successive integer, whether or not the proposal is adopted.

If a rule is repealed and reenacted, it receives the number of the proposal to reenact it. If a rule is amended or transmuted, it receives the number of the proposal to amend or transmute it. If an amendment is amended or repealed, the entire rule of which it is a part receives the number of the proposal to amend or repeal the amendment.

**109.** Rule-changes that transmute immutable rules into mutable rules may be adopted if and only if the vote is unanimous among the eligible voters. Transmutation shall not be implied, but must be stated explicitly in a proposal to take effect.

**110.** In a conflict between a mutable and an immutable rule, the immutable rule takes precedence and the mutable rule shall be entirely void. For the purposes of this rule a proposal to transmute an immutable rule does not "conflict" with that immutable rule.

**111.** If a rule-change as proposed is unclear, ambiguous, paradoxical, or destructive of play, or if it arguably consists of two or more rule-changes compounded or is an amendment that makes no difference, or if it is otherwise of questionable value, then the other players may suggest amendments or argue against the proposal before the vote. A reasonable time must be allowed for this debate. The proponent decides the final form in which the proposal is to be voted on and, unless the Judge has been asked to do so, also decides the time to end debate and vote.

**112.** The state of affairs that constitutes winning may not be altered from achieving _n_ points to any other state of affairs. The magnitude of _n_ and the means of earning points may be changed, and rules that establish a winner when play cannot continue may be enacted and (while they are mutable) be amended or repealed.

**113.** A player always has the option to forfeit the game rather than continue to play or incur a game penalty. No penalty worse than losing, in the judgment of the player to incur it, may be imposed.

**114.** There must always be at least one mutable rule. The adoption of rule-changes must never become completely impermissible.

**115.** Rule-changes that affect rules needed to allow or apply rule-changes are as permissible as other rule-changes. Even rule-changes that amend or repeal their own authority are permissible. No rule-change or type of move is impermissible solely on account of the self-reference or self-application of a rule.

**116.** Whatever is not prohibited or regulated by a rule is permitted and unregulated, with the sole exception of changing the rules, which is permitted only when a rule or set of rules explicitly or implicitly permits it.

## Mutable Rules

**201.** Players shall alternate in clockwise order, taking one whole turn apiece. Turns may not be skipped or passed, and parts of turns may not be omitted. All players begin with zero points.

In mail and computer games, player order shall be determined by dice roll, with the highest roll going first and continuing in descending order. In the event of a tie, rerolls are performed until an unambiguous order is achieved.

**304.** One turn consists of two parts in this order: (1) proposing one rule-change and having it voted on, and (2) throwing one six sided die once and adding the number of points on its face to one's score. If the proposed rule-change contains an appropriate amount of whimsy the player may add an additional point to their score on that turn. Exemplary whimsy includes but is not limited to relevant or amusing acronyms in the rule name and prominent and relevant limericks, rhyming couplets or similar poetic devices in the text.

Disputes over whimsicality are to be settled via the Fool Judge, initially the player whose turn shall be next. The Fool Judge is officially obliged to offer unbiased adjudication, and their final decision can be opposed only by a unanimous vote against from all other players not currently taking a turn. If the Fool Judge's decision is thus opposed, the next available player becomes Fool Judge. The current player may not be the Fool Judge. If every other player has unsuccessfully been Fool Judge a Court of Fools is declared and the current rule-change is allowed whimsy status.

\[Previously 202\]

**203.** A rule-change is adopted if and only if the vote is unanimous among the eligible voters. If this rule is not amended by the end of the second complete circuit of turns, it automatically changes to require only a simple majority.

**204.** If and when rule-changes can be adopted without unanimity, the players who vote against winning proposals shall receive 10 points each.

**205.** An adopted rule-change takes full effect at the moment of the completion of the vote that adopted it.

**206.** When a proposed rule-change is defeated, the player who proposed it loses 10 points.

**207.** Each player always has exactly one vote.

**208.** The winner is the first player to achieve 100 (positive) points.

**209.** At no time may there be more than 25 mutable rules.

**210.** Players may not conspire or consult on the making of future rule-changes unless they are team-mates.

The first paragraph of this rule does not apply to games by mail or computer.

**211.** If two or more mutable rules conflict with one another, or if two or more immutable rules conflict with one another, then the rule with the lowest ordinal number takes precedence.

If at least one of the rules in conflict explicitly says of itself that it defers to another rule (or type of rule) or takes precedence over another rule (or type of rule), then such provisions shall supersede the numerical method for determining precedence.

If two or more rules claim to take precedence over one another or to defer to one another, then the numerical method again governs.

**307.** If players disagree about the legality of a move or the interpretation or application of a rule, then the player preceding the one moving is to be the Judge and decide the question. Disagreement for the purposes of this rule may be created by the insistence of any player. This process is called invoking Judgment.

When Judgment has been invoked, the next player may not begin his or her turn without the consent of a majority of the other players.

The Judge's Judgment may be overruled only by a two thirds majority vote taken before the next turn is begun. If a Judge's Judgment is overruled, then the player preceding the Judge in the playing order becomes the new Judge for the question, and so on, except that no player is to be Judge during his or her own turn or during the turn of a team-mate.

Unless a Judge is overruled, one Judge settles all questions arising from the game until the next turn is begun, including questions as to his or her own legitimacy and jurisdiction as Judge.

New Judges are not bound by the decisions of old Judges. New Judges may, however, settle only those questions on which the players currently disagree and that affect the completion of the turn in which Judgment was invoked. All decisions by Judges shall be in accordance with all the rules then in effect; but when the rules are silent, inconsistent, or unclear on the point at issue, then the Judge shall consider game-custom and the spirit of the game before applying other standards

[Previously 212]

**213.** If the rules are changed so that further play is impossible, or if the legality of a move cannot be determined with finality, or if by the Judge's best reasoning, not overruled, a move appears equally legal and illegal, then the first player unable to complete a turn is the winner.

This rule takes precedence over every other rule determining the winner.

**301.** ""The Title Rule"" Every new rule proposed after the enactment of this rule must be given a title or name by the player who proposed it.

This will be done by listing the title or name in the beginning of the text of the rule, between double quotation marks for clarity.

This rule will be referred to as ""The title rule"".

**302.** ""The The Rule Rule"" When a rule is proposed, if its Title (per The Title Rule) does not start with "The" and end with "Rule", all players must vote against it (Including the player that proposed it).

**303.** ""The "How a Bill Becomes a Law" Rule""
Any new proposal must be proposed (as per 202.1) through either of the two following paths:

- **Path A. Discord:**

1. _The proposing player (proposer)_ must post a _draft proposal (draft)_ to the thread Nomic Proposals . The draft must contain the full text of the intended proposal.
2. Other players may discuss the _draft_ in the thread Nomic! and suggest changes for _the proposer_ to implement.
3. The proposer may amend the _draft_ by editing the message containing the draft in Nomic Proposals , or by posting a new _draft_ and deleting the old _draft_.
4. _The proposer_ must clearly and unambiguously announce in the thread Nomic Proposals when the _draft_ is finished by posting a new message thus making it a proposal. No further changes to the proposal may happen after this unless rule 111 has been invoked. This may happen immediately after 303.A.1.

- **Path B. GitLab:**

1. The _proposing player (proposer)_ must create a _merge request_ into branch `main` of the Milk Nomic GitLab repository.
2. A link to the merge request must be posted in the discord thread Nomic Proposals with a brief summary of the changes.
3. Other players may discuss the _merge request_ in the discord thread Nomic! or by commenting in the merge request and suggest changes for the proposer to implement.
4. The proposer may amend the _merge request_ by adding new commits to the _merge request_.
5. _The proposer_ must clearly and unambiguously announce in the thread Nomic Proposals when _merge request_ is finished by posting a new message thus making it a proposal. No further commits to the _merge request_ may happen after this unless rule 111 has been invoked. This may happen immediately after step 303.B.2.
6. If the proposal is adopted the _merge request_ must be merged, if not - closed without merging.

**308.** ""The Vote Obligation Roll Enactment"" Rule
A new step is added to each players turn, before a rule change is proposed. In this step, the active player rolls a die. Based on the result of that roll, certain players may be obligated to vote for the rule change that the active player proposes in their turn (deferring to rule 302 if conflicting), as follows -
1: Sonja
2: Jbt
3: Sate
4: Coinky
5: Haunty
6: Ned

This initial ordering is made to correspond to the turn order of the players. Whenever a player leaves, or a new player joins, this ordering must be rearranged. To do so, all players may submit a suggested reordering (outside of normal rule change proposal structure), and all players must vote on all suggested reorderings (not beholden to rules pertaining specifically to rule change voting). The most popular reordering will immediately replace the text of the previous ordering of roll results and obligated voters, without changing this rule's number. If there is a tie among the most popular reordering proposals, vote again up to 2 times, before rolling a die to break it if necessary.

**309.** "The OBAMNA (Obtaining Brevity and Agility with Magnificent Nominative Action) Rule"
At the beginning of each circuit of turns a player shall be elected by a majority vote among the eligible voters for the office of the Nominal President. No player may serve as the Nominal President for more than 2 consecutive circuits of turns. If during the election no eligible player receives a majority of the votes the office shall stay vacant until the next election.

The Nominal President's duties are

- ensure swift progress of the game by reminding the players to perform the actions necessary to progress the game - publishing proposals, voting, publishing finished rules into appropriate channels etc.
- ensure that the rules and customs of the game are being followed by all players.

The Nominal President may, in order to perform their duties

- allow players to switch their order of turns until the end of current circuit of turns, with consent from both players
- declare that a draft proposal is finished and can be voted on, if the player who has drafted the proposal does not make any changes to it for at least 24 hours
- declare that a player is not present for the vote if the player hasn't yet voted and 24 hours have passed since the start of voting. If the player is obligated by the rules to vote in a particular way, the Nominal President may cast that vote on the absent player's behalf, otherwise absent vote is equivalent to a "No"
- punish players who break other customs of the game in a way that is detrimental to the progress of the game by taking away 1 point from their score, this punishment may be enacted no more than once per player per presidential term, and can be reversed by a Judge until the next turn has started

If the Nominal President is perceived to be abusing their powers or insufficiently performing their duties an impeachment of the Nominal President may be declared if all players except for the one currently occupying the office of the Nominal President unanimously react to a message suggesting the impeachment with a peach emote (🍑). When impeachment is declared the Nominal President is relieved of their duties, they must subtract 1d6 from their score and they must place a peach emote (🍑) in their Discord username until the next Nominal President is elected. The office shall stay vacant until the next election.

**310.** "The Secretive Officiation of Democratic Applications (SODA) Rule"

There once was a vote held in Nomic,
Yet nobody knows what was on it.
First at rule's behest,
Were choices digressed -
Our bot held the count electronic 

All rule change votes shall be held such that the above limerick may be accurately applied to them. The second line need not apply to one's own vote, or to votes decided through a rule such as The The Rule Rule or The OBAMNA Rule.

Tihe default interpretation of the voting process, unless collectively agreed to be otherwise, is that votes be sent to a bot, which then announces the final result (as well as potentially who has/has not yet voted at e.g. the OBAMNA President's request). Players may then request to verify their claim to have voted a certain way if it directly interacts with a rule, e.g. for voting against a passed proposal as per rule 204, or to prove they voted in accordance with a mandated vote. Good faith is assumed of the bot manager. 

**312.** "The 'Codification Of Developer Involvement (in) Nomic Gameplay' (CODING) Rule"

All proposals submitted after this rule which involve the creation or alteration of software designed for use during gameplay will be submitted with the acknowledgement and understanding that such software is written by volunteers in whatever time they choose to devote to it. In so choosing to volunteer their labour towards this end, the software developer(s) will endeavour to provide the necessary software functionality in a timely fashion. 

If the absence or technical failure of any required software impedes gameplay for more than 48 hours then one of two methods may be followed:

Method 1: A rule proposal may be written with a fallback-procedure which details how to proceed without the required software. If this approach is taken, then method 2 may not be used.
Method 2: The players may decide, with the agreement of unanimous minus one players, how to proceed without the required software 

**313.** ""The Establishment of Agricultural Territory rule""

Players are now obliged to produce food for the realm. They must choose one of the following food groups to specialize in: Fruits, root vegetables, and grains. Depending on the choice, it will cause one of the following effects for the player who chose it:
**Fruits** - Every time a rule-change passes voting, gain 1 point.
**Root vegetables** - If your own rule-change is defeated, gain 6 points. This happens after rule 206.
**Grains** - At the end of your turn, gain 3 points.
A player will also gain 1 token symbolizing their chosen food group immediately after rolling for points. This token is tradeable between players, as long as both parties have explicitly written that they consent to a trade.
Players will choose a food group to specialize in immediately following the passing of this rule (before rolling for points). If any player is not specialized in a food group, they will choose one at the start of their turn, before they would propose a rule. If a player wishes to specialize in a different food group, they may do so on their turn before proposing a rule, however it will cost a penalty of 5 points.

Players may propose "recipes" that utilize food tokens. These recipes cost food tokens to gain an amount of points. The food tokens are removed from the game after use for creating a recipe. They can consist of any amount of food tokens, however must involve at least two different food groups. They cannot give more points than the amount of food tokens it costs to the power of two.
Proposing recipes can be done on the proposing player's turn, but may not be done while proposing a rule or after rolling for points. If a recipe is proposed, a vote will be held, and all players must participate by casting votes. If a majority of players vote to keep the recipe, it will be added to this rule, along with its effect.

*While the nobles pass rules and laws,
Treating their duty as a game to win,
The people long for food in their jaws,
Lust for power would lead to grave sin.*

**314.** ""The Lesser Rule""

"Pacts" are binding agreements that can stipulate participating members to perform certain actions, such as:

Vote in a way mandated by the pact
Propose a certain proposal that is mandated by the pact
Exchange points between each other
Other actions not prohibited by the rules

A pact requires at least two members. Members of the pact must follow the obligations in the pact as long as they remain members of it unless a pact conflicts with a rule. When entering a pact all members must be aware of and consent to the entire text of the pact. To enter a pact the participating players must collectively pay a cost of at least 1 point per p|ayer. This cost is not gained by another player. Instead, the cost paid is removed from the game completely.

When written, a pact must always specify at least one way to leave the pact, available to all pact members, which may require certain actions be taken to complete. When no further actions could possibly be compelled as a result of the pact the pact is considered complete and nullified. Leaving the pact and any actions taken to do so are not considered "actions" for the purposes of this clause.

When a pact would require a member to do something that would violate the rules, they must follow the rules rather than the pact.  If such conflict arises and no legal action, including utilizing the pact-breaking clause, may resolve the conflict, pact members must temporarily ignore the pact (exclusively regarding the particular rule conflict; the pact is not nullified). When multiple players could resolve a conflict using legal actions, the player whose actions are compelled must do it before any other members of the pact, if able.

When exchanging points, paying points for leaving the pact, or otherwise deducting points from their score due to a pact, players are not allowed to end up with negative points after performing the deduction, and would fail to accomplish the action. 
