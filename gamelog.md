# Game Log

## Round 1
- Sonja proposed **301. ""The title rule""**
- **301. The title rule** passed with 6 ayes - 0 noes
- Sonja rolled 1d6 scoring 2 points

- jbt proposed **302. ""The The Rule Rule""**
- **302. The The Rule Rule** passed with 6 ayes - 0 noes
- jbt rolled 1d6 scoring 5 points

- Sateviss proposed **303. ""The "How a Bill Becomes a Law" Rule""**
- **303. The "How a Bill Becomes a Law" Rule** passed with 6 ayes- 0 noes
- Sateviss rolled 1d6 scoring 4 points

- Coinkydink proposed **304. Amendment to Rule 202**
- **304. Amendment to Rule 202** passed with 6 ayes - 0 noes
- Coinkydink rolled 1d6 scoring 3 points

- Haunty proposed **305. "The Nihilism Rule"**
- **305. "The Nihilism Rule"** rejected with 5 ayes - 1 no
- Haunty gained 1 point as a result of rule 304
- Haunty loses 10 points as a result of rule 206
- Haunty rolled 1d6 scoring 3 points

- Ned proposed **306. "The Punctuality Rule"**
- **306. "The Punctuality Rule"** rejected with 5 ayes - 1 no
- Ned gained 1 point as a result of rule 304
- Ned loses 10 points as a result of rule 206
- Ned rolled 1d6 scoring 5 points

## Round 2
- Sonja proposed **307. Amedment to Rule 212**
- **307. Amendment to Rule 212** passed with 6 ayes - 0 noes
- Sonja rolled 1d6 scoring 5 points

- jbt proposed **308. ""The Vote Obligation Roll Enactment Rule""**
- **308. ""The Vote Obligation Roll Enactment Rule""** passed with 6 ayes - 0 noes
- jbt gained 1 point as a result of rule 304
- jbt rolled 1d6 scoring 2 points

- Sateviss proposed **309. "The OBAMNA (Obtaining Brevity and Agility with Magnificent Nominative Action) Rule"**
- **309. "The OBAMNA (Obtaining Brevity and Agility with Magnificent Nominative Action) Rule"** passed with 6 ayes - 0 noes
- Sateviss gained 1 point as a result of rule 304
- Sateviss rolled 1d6 scoring 4 points

- Coinykdink proposed **310. "The Secretive Officiation of Democratic Applications (SODA) Rule"**
- **310. "The Secretive Officiation of Democratic Applications (SODA) Rule"!** passed with 6 ayes - 0 noes
- Coinkydink gained 1 point as result of rule 304
- Coinkydink rolled 1d6 scoring 6 points

- Haunty proposed **311. The "The 'No News Is Bad News' Rule" Rule.** 
- **311. The "The 'No News Is Bad News' Rule" Rule.** rejected
- Haunty gained 1 point as a result of rule 304
- Haunty loses 10 points as a result of rule 206
- Haunty rolled 1d6 scoring 5 points

- Ned proposed **312. "The 'Codification Of Developer Involvement (in) Nomic Gameplay' (CODING) Rule"**
- **312. "The 'Codification Of Developer Involvement (in) Nomic Gameplay' (CODING) Rule"** accepted
- Ned gained 1 point as a result of rule 304
- Ned rolled 1d6 scoring 6 points

- Sonja proposed **313. ""The Establishment of Agricultural Territory rule""**
- **313. ""The Establishment of Agricultural Territory rule""** accepted
- Sonja chose grains for rule 313
- Haunty chose root vegetables for rule 313
- Ned chose fruits for rule 313
- jbt chose fruits for rule 313
- Sateviss chose grains for rule 313
- Coinkydink chose fruits for rule 313
- Sonja gained 1 point as a result of rule 304
- Sonja rolled 1d6 scoring 6 points
- Sonja gained 3 points from grains

- jbt proposed **314. ""The Lesser Rule""**
- **314. ""The Lesser Rule""** accepted
- Coinkydink gained 10 points for voting against a passing proposal
- Haunty gained 10 points for voting against a passing proposal
- Coinkydink gained 1 point from fruit
- jbt gained 1 point from fruit
- Ned gained 1 point from fruit
- jbt rolled 1d6 scoring 6 points

